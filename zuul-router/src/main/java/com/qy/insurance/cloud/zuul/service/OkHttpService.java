package com.qy.insurance.cloud.zuul.service;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @Description
 * @Author yangshuai
 * @Date 2018/5/26
 * @ModifiedBy
 * @Version v1.0
 */
@Component
@Slf4j
@Data
public class OkHttpService {
    @Autowired
    private OkHttpClient okHttpClient;


    /**
     * @Description Post请求发送JSON数据
     * @Author yangshuai
     * @Date 2018/5/29
     * @Param [url, jsonParams, headerMap]
     * @Return java.lang.String
     * @ModifiedBy
     * @Exception
     */
    public String postJsonParams(String url, String jsonParams, Map<String, String> headerMap) {
        String responseBody = "";
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonParams);
        Request.Builder builder = new Request.Builder();
        if (headerMap != null) {
            //添加头部信息
            for (Map.Entry<String, String> entry : headerMap.entrySet()) {
                builder.addHeader(entry.getKey(), entry.getValue());
            }
        }
        Request request = builder.url(url).post(requestBody).build();
        Response response = null;
        try {
            response = this.okHttpClient.newCall(request).execute();
            if (response.isSuccessful()) {
                return response.body().string();
            }
        } catch (Exception e) {
            log.error("okhttp3 post by json >> ex = {}", e);
        } finally {
            if (response != null) {
                response.close();
            }
        }
        return responseBody;
    }
}

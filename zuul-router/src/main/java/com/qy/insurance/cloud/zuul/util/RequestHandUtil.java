package com.qy.insurance.cloud.zuul.util;

import com.netflix.zuul.context.RequestContext;
import com.qy.insurance.cloud.zuul.entity.RequestInfo;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @Description
 * @Author yangshuai
 * @Date 2018/5/29
 * @ModifiedBy
 * @Version v1.0
 */
@Slf4j
public class RequestHandUtil {

    /**
     * @Description 获取request请求方式
     * @Author yangshuai
     * @Date 2018/5/29
     * @Param [request]
     * @Return java.lang.String
     * @ModifiedBy
     * @Exception
     */
    public static String getVerb(HttpServletRequest request) {
        String method = request.getMethod();
        return method == null ? "GET" : method;
    }

    /**
     * @Description 获取requestBody中的json
     * @Author yangshuai
     * @Date 2018/5/29
     * @Param [request]
     * @Return java.io.InputStream
     * @ModifiedBy
     * @Exception
     */
    public static InputStream getRequestBody(HttpServletRequest request) {
        Object requestEntity = null;

        try {
            requestEntity = RequestContext.getCurrentContext().get("requestEntity");
            if (requestEntity == null) {
                requestEntity = request.getInputStream();
            }
        } catch (IOException var4) {
            log.error("Error during getRequestBody", var4);
        }

        return (InputStream) requestEntity;
    }

    /**
     * @Description 构造RequestInfo对象
     * @Author yangshuai
     * @Date 2018/5/29
     * @Param [context]
     * @Return com.qy.insurance.cloud.zuul.entity.RequestInfo
     * @ModifiedBy
     * @Exception
     */
    public static RequestInfo getRequestInfo(RequestContext context) {
        RequestInfo requestInfo = new RequestInfo();
        HttpServletRequest request = context.getRequest();
        String serviceId = (String) context.get("serviceId");
        String zuulTimes = request.getParameter("zuulTimes");
        //解析json入参，获取json入参
        InputStream requestEntity = RequestHandUtil.getRequestBody(request);
        BufferedReader br = new BufferedReader(new InputStreamReader(requestEntity));
        StringBuilder requestJson = new StringBuilder();
        String temp;
        try {
            while ((temp = br.readLine()) != null) {
                requestJson.append(temp);
            }
        } catch (Exception e) {
            log.error("{}队列请求json入参解析失败，uri为：{}", serviceId, request.getRequestURI());
        } finally {
            try {
                requestEntity.close();
            } catch (IOException e) {
                log.error("流关闭异常 {}", e);
            }
        }

        //请求保存
        requestInfo.setServiceId(serviceId);
        requestInfo.setUri(request.getRequestURI());
        requestInfo.setZuulTimes(zuulTimes);
        requestInfo.setRequestJson(requestJson.toString());
        return requestInfo;
    }
}

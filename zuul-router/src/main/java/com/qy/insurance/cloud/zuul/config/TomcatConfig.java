package com.qy.insurance.cloud.zuul.config;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description Tomcat服务器配置
 * @Author yangshuai
 * @Date 2018/5/25
 * @ModifiedBy
 * @Version v1.0
 */
@Configuration
public class TomcatConfig {
    @Value("${tomcat-config.port}")
    int port;

    @Value("${tomcat-config.max-connections}")
    int maxConnections;

    @Value("${tomcat-config.max-threads}")
    int maxThreads;

    @Value("${tomcat-config.min-spareThreads}")
    int minSpareThreads;

    @Value("${tomcat-config.connection-timeout}")
    int connectionTimeout;

    @Bean
    public EmbeddedServletContainerFactory createEmbeddedServletContainerFactory() {
        TomcatEmbeddedServletContainerFactory tomcatFactory = new TomcatEmbeddedServletContainerFactory();
        tomcatFactory.setPort(this.port);
        tomcatFactory.addConnectorCustomizers(new MyTomcatConnectorCustomizer());
        return tomcatFactory;
    }

    class MyTomcatConnectorCustomizer implements TomcatConnectorCustomizer {
        public void customize(Connector connector) {
            Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();
            //设置最大连接数
            protocol.setMaxConnections(TomcatConfig.this.maxConnections);
            //设置最大线程数
            protocol.setMaxThreads(TomcatConfig.this.maxThreads);
            protocol.setMinSpareThreads(TomcatConfig.this.minSpareThreads);
            protocol.setConnectionTimeout(TomcatConfig.this.connectionTimeout);
        }
    }
}



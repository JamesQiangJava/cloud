package com.qy.insurance.cloud.zuul.service;

import com.google.common.util.concurrent.RateLimiter;
import com.qy.insurance.cloud.zuul.constant.ServiceEnum;
import com.qy.insurance.cloud.zuul.entity.RequestInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedTransferQueue;

/**
 * @Description 限速拦截器服务类
 * @Author yangshuai
 * @Date 2018/5/21
 * @ModifiedBy
 * @Version v1.0
 */
@Component
@Slf4j
public class RateLimitService {
    @Autowired
    ConsumeService consumeService;
    //服务速率rateLimiter
    private Map<String, ServiceProperties> rateLimiterMap = new HashMap<>();
    //配置文件路径
    private String propsPath = "serviceRate.properties";
    //服务线程池
    private static ExecutorService executorService = Executors.newFixedThreadPool(10);

    /**
     * @Description 初始化类，从配置文件中读取各服务的速率配置
     * @Author yangshuai
     * @Date 2018/5/22
     * @Param []
     * @Return void
     * @ModifiedBy
     * @Exception
     */
    @PostConstruct
    private void init() {
        log.info("各服务RateLimiter初始化开始");
        //从配置文件加载
        try {
            Properties props = PropertiesLoaderUtils.loadAllProperties(this.propsPath);
            for (Object objectKey : props.keySet()) {
                String key = (String) objectKey;
                // 读入各服务限速
                double rate = Double.parseDouble(props.getProperty(key));
                this.rateLimiterMap.put(key, new ServiceProperties(RateLimiter.create(rate)));
                //为各个服务启动一个线程消费
                executorService.execute(() -> {
                    while (true) {
                        ServiceProperties serviceProperties = this.rateLimiterMap.get(key);
                        Queue<RequestInfo> requestQueue = serviceProperties.getRequestQueue();
                        RateLimiter rateLimiter = serviceProperties.getRateLimiter();
                        while (!requestQueue.isEmpty()) {
                            RequestInfo requestInfo = requestQueue.poll();
                            String serviceId = requestInfo.getServiceId();
                            double waitTime = rateLimiter.acquire();
                            String threadName = Thread.currentThread().getName();
                            log.info("{}待处理业务数量为{}个，消费线程为{}", serviceId, requestQueue.size(), threadName);
                            log.info("{}等待{}秒后，获取令牌，消费线程为{}", serviceId, waitTime, threadName);
                            serviceProperties.executorService.execute(() -> {
                                consumeService.consumeRequest(requestInfo);
                            });
                        }
                        synchronized (ServiceEnum.getServiceById(key)) {
                            try {
                                ServiceEnum serviceEnum = ServiceEnum.getServiceById(key);
                                serviceEnum.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                log.info("{} 启动消费线程完成，消费速率为{}/s", key, rate);
            }
        } catch (
                IOException e)

        {
            log.error("读取 {} 文件发生异常，各服务RateLimiter初始化未成功", propsPath);
        }
        log.info("各服务RateLimiter初始化结束");
    }


    /**
     * @Description 添加请求上下文到服务队列
     * @Author yangshuai
     * @Date 2018/5/29
     * @Param [requestInfo]
     * @Return void
     * @ModifiedBy
     * @Exception
     */
    public void addRequest(RequestInfo requestInfo) {
        String serviceId = requestInfo.getServiceId();
        Queue<RequestInfo> requestQueue = this.rateLimiterMap.get(serviceId).getRequestQueue();
        requestQueue.offer(requestInfo);
        //有消息入队后激活轮询线程
        synchronized (ServiceEnum.getServiceById(serviceId)) {
            ServiceEnum serviceEnum = ServiceEnum.getServiceById(serviceId);
            serviceEnum.notify();
        }
    }

    /**
     * @Description 更新serviceId对应的服务速率配置
     * @Author yangshuai
     * @Date 2018/5/29
     * @Param [serviceId, rate]
     * @Return void
     * @ModifiedBy
     * @Exception
     */
    public void setServiceRateLimiter(String serviceId, double rate) {
        //更新服务限速
        this.rateLimiterMap.get(serviceId).setRateLimiter(RateLimiter.create(rate));
        //更新配置文件
        try {
            Properties props = PropertiesLoaderUtils.loadAllProperties(this.propsPath);
            //写入配置文件
            props.setProperty(serviceId, String.valueOf(rate));
        } catch (IOException e) {
            log.error("读取 {} 文件发生异常", propsPath);
        }
    }

    /**
     * @Description 服务限流配置类
     * @Author yangshuai
     * @Date 2018/5/21
     * @ModifiedBy
     * @Version v1.0
     */
    public class ServiceProperties {
        private RateLimiter rateLimiter;
        private Queue<RequestInfo> requestQueue = new LinkedTransferQueue<RequestInfo>();
        //服务分发线程池
        private ExecutorService executorService = Executors.newFixedThreadPool(10);

        ServiceProperties(RateLimiter rateLimiter) {
            this.rateLimiter = rateLimiter;
        }

        public RateLimiter getRateLimiter() {
            return this.rateLimiter;
        }

        public void setRateLimiter(RateLimiter rateLimiter) {
            this.rateLimiter = rateLimiter;
        }

        public Queue<RequestInfo> getRequestQueue() {
            return this.requestQueue;
        }
    }
}

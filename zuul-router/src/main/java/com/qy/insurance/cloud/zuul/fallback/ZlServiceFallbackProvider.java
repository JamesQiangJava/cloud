package com.qy.insurance.cloud.zuul.fallback;

import com.netflix.zuul.context.RequestContext;
import com.qy.insurance.cloud.zuul.dto.CommonResponse;
import com.qy.insurance.cloud.zuul.service.RateLimitService;
import com.qy.insurance.cloud.zuul.util.RequestHandUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.netflix.zuul.filters.route.ZuulFallbackProvider;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @Author yangshuai
 * @Date 2018/5/25
 * @ModifiedBy
 * @Version v1.0
 */
@Component
@Slf4j
@Data
@ConfigurationProperties(prefix = "zuul.routes.zl")
public class ZlServiceFallbackProvider implements ZuulFallbackProvider {

    @Autowired
    private RateLimitService rateLimitService;
    private String serviceId;

    @Override
    public String getRoute() {
        return serviceId; //api服务id，如果需要所有调用都支持回退，则return "*"或return null
    }

    /**
     * @Description 如果请求用户服务失败，返回什么信息给消费者客户端
     * @Author yangshuai
     * @Date 2018/5/23
     * @Param []
     * @Return org.springframework.http.client.ClientHttpResponse
     * @ModifiedBy
     * @Exception
     */
    @Override
    public ClientHttpResponse fallbackResponse() {
        //放入队列,降级处理
        RequestContext context = RequestContext.getCurrentContext();
        if (context.getRequest().getRequestURI().endsWith("/getPatientData")) {
            //加入队列处理
            log.info("熔断器熔断,将请求加入{}服务的队列，延迟处理", serviceId);
            rateLimitService.addRequest(RequestHandUtil.getRequestInfo(context));
            return CommonResponse.getClientHttpResponse(true, serviceId);
        } else {
            log.info("熔断器熔断，{}降级处理", serviceId);
            return CommonResponse.getClientHttpResponse(false, serviceId);
        }
    }
}

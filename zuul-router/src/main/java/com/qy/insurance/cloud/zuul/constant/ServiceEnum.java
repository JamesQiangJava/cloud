package com.qy.insurance.cloud.zuul.constant;

import org.apache.commons.lang.StringUtils;

/**
 * @Description service枚举，上锁用
 * @Author yangshuai
 * @Date 2018/5/24
 * @ModifiedBy
 * @Version v1.0
 */
public enum ServiceEnum {
    BUSINESS("demo-role-authority"),
    LINK("customer_link"),
    ZL("insurance-lp-zl"),
    ZP("insurance-lp-zp"),
    LS("insurance-lp-ls"),
    HB("insurance-hb"),
    HBSURVEY("hb-survey"),
    LPSURVEY("lp-survey"),
    DP("insurance-direct-pay");

    private String serviceId;

    ServiceEnum(String serviceId) {
        this.serviceId = serviceId;
    }

    /**
     * @Description 通过serviceId获取服务枚举
     * @Author yangshuai
     * @Date 2018/5/25
     * @Param [serviceId]
     * @Return com.qy.insurance.cloud.zuul.constant.ServiceEnum
     * @ModifiedBy
     * @Exception
     */
    public static ServiceEnum getServiceById(String serviceId) {
        for (ServiceEnum serviceEnum : ServiceEnum.values()) {
            if (StringUtils.equals(serviceId, serviceEnum.serviceId)) {
                return serviceEnum;
            }
        }
        return null;
    }
}

package com.qy.insurance.cloud.zuul.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qy.insurance.cloud.zuul.util.CipherUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @Description Common Response
 * @Author yangshuai
 * @Date 2018/4/25
 * @ModifiedBy
 * @Version v1.0
 */
@Data
@Slf4j
public class CommonResponse<T> {
    /**
     * 返回状态
     */
    private String status; //提示信息标志 SUCCESS:请求成功 FAIL:请求失败
    /**
     * 返回编码
     */
    private String responseCode; //请求返回状态码 200:请求成功
    /**
     * 返回信息
     */
    private String message;  //请求返回提示信息
    /**
     * 请求返回时间
     */
    private String time; //yyyy-MM-dd HH:mm:ss
    /**
     * 公司编码
     */
    private String companyCode = "QUYI"; //公司编码
    /**
     * 交易流水号
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String flowId;
    /**
     * 保险公司编码
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String clientCode;
    /**
     * 返回数据对象
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T data;

    /**
     * @Description 构造通用成功返回结果
     * @Author yangshuai
     * @Date 2018/5/29
     * @Param [t, successMessage, flowId]
     * @Return com.qy.insurance.cloud.zuul.dto.CommonResponse<T>
     * @ModifiedBy
     * @Exception
     */
    public static <T> CommonResponse<T> makeSuccessResult(T t, String successMessage, String flowId) {
        CommonResponse response = new CommonResponse();
        response.setStatus("SUCCESS");
        response.setResponseCode("200");
        response.setMessage(successMessage);
        response.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        response.setFlowId(flowId);
        response.setData(t);
        return response;
    }

    /**
     * @Description 构造通用失败返回结果
     * @Author yangshuai
     * @Date 2018/5/29
     * @Param [t, errorMessage, flowId]
     * @Return com.qy.insurance.cloud.zuul.dto.CommonResponse<T>
     * @ModifiedBy
     * @Exception
     */
    public static <T> CommonResponse<T> makeErrorResult(T t, String errorMessage, String flowId) {
        CommonResponse response = new CommonResponse();
        response.setStatus("FAIL");
        response.setResponseCode("500");
        response.setMessage(errorMessage);
        response.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        response.setFlowId(flowId);
        response.setData(t);
        return response;
    }

    /**
     * @Description 构造降级处理响应json
     * @Author yangshuai
     * @Date 2018/5/29
     * @Param [isPush, serviceId]
     * @Return java.lang.String
     * @ModifiedBy
     * @Exception
     */
    public static String getResponseJson(boolean isPushQueue, String serviceId) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        String responseJson = "";
        if (isPushQueue && serviceId.equals("insurance-lp-zl")) {
            ZlCommonResponse<List> zlCommonResponse = ZlCommonResponse.makeSuccessResult(null, "操作成功。");
            String temp = ZlCommonResponse.toPackage(zlCommonResponse);
            responseJson = CipherUtil.build("AES", "rh6n8jaUURIQ1BjQnLbdnaZtr54SCQ7R", "UTF-8").encryptString(temp);
            log.info("极速赔异步接口降级处理，响应成功");
        } else if (serviceId.equals("insurance-lp-zl")) {
            ZlCommonResponse<List> zlCommonResponse = ZlCommonResponse.makeErrorResult(null, "获取lp-zl数据失败");
            String temp = ZlCommonResponse.toPackage(zlCommonResponse);
            responseJson = CipherUtil.build("AES", "rh6n8jaUURIQ1BjQnLbdnaZtr54SCQ7R", "UTF-8").encryptString(temp);
            log.info("极速赔同步接口降级处理，响应成功");
        } else {
            CommonResponse<Object> commonResponse = CommonResponse.makeErrorResult(null, "服务器异常，请稍后再试", null);
            try {
                responseJson = objectMapper.writeValueAsString(commonResponse);
            } catch (Exception e) {
                log.info("json转换异常");
            }
        }
        return responseJson;
    }

    /**
     * @Description 构造熔断响应
     * @Author yangshuai
     * @Date 2018/5/29
     * @Param [isPush, serviceId]
     * @Return org.springframework.http.client.ClientHttpResponse
     * @ModifiedBy
     * @Exception
     */
    public static ClientHttpResponse getClientHttpResponse(boolean isPushQueue, String serviceId) {
        return new ClientHttpResponse() {
            @Override
            public InputStream getBody() throws IOException {
                //构造响应
                String responseJson = CommonResponse.getResponseJson(isPushQueue, serviceId);
                return new ByteArrayInputStream(responseJson.getBytes("UTF-8"));
            }

            @Override
            public HttpHeaders getHeaders() {
                HttpHeaders headers = new HttpHeaders();
                //和body中的内容编码一致，否则容易乱码
                headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
                return headers;
            }

            @Override
            public HttpStatus getStatusCode() {
                return HttpStatus.OK;
            }

            @Override
            public int getRawStatusCode() {
                return HttpStatus.OK.value();
            }

            @Override
            public String getStatusText() {

                return HttpStatus.OK.getReasonPhrase();
            }

            @Override
            public void close() {

            }
        };
    }

}

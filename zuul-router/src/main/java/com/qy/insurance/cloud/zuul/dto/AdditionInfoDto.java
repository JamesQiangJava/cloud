package com.qy.insurance.cloud.zuul.dto;

import lombok.Data;

/**
 * @Description
 * @Author yangshuai
 * @Date 2018/5/28
 * @ModifiedBy
 * @Version v1.0
 */
@Data
public class AdditionInfoDto {
    /**
     * 错误代码
     */
    private String errorCode;
    /**
     * 错误提示
     */
    private String errorMsg;
    /**
     * 接收方交易流水号
     */
    private String receiverTradeNum;
    /**
     * 一串数字用于标识消息以防这条消息被修改
     */
    private String correlationId;
    /**
     * 用于标示是否采用异步请求机制
     */
    private String asyncAsk;
    /**
     * 异步请求后的回调地址，供平台调用返回结果报文
     */
    private String callback;
    /**
     * 异步请求后的回调地址，供动态库调用返回结果报文
     */
    private String curDllAddr;
}

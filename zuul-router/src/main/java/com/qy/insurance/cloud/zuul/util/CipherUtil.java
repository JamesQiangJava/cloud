/*
 * Copyright (c) 2016. 上海趣医网络科技有限公司 版权所有
 * Shanghai QuYi Network Technology Co., Ltd. All Rights Reserved.
 *
 * This is NOT a freeware,use is subject to license terms.
 *
 */

package com.qy.insurance.cloud.zuul.util;

import lombok.extern.slf4j.Slf4j;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.security.KeyFactory;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

/**
 * @task: BXSALE-986
 * @discrption: Cipher support text message encryption and decryption
 * @author: Aere
 * @date: 2016/11/7 15:12
 * @version: 1.0.0
 * @extra: ##########         How to generate RSA key pair         ##########
 * 1. Generate original private key of 2^n bit length
 * openssl genrsa -out private.pem 2048
 * 2. Export public key of X509 Encoded format
 * openssl rsa -in private.pem -out public_key.pem -pubout
 * 3. Export private key of pkcs8 Encoded format
 * openssl pkcs8 -topk8 -in private.pem -out pkcs8_private_key.pem -nocrypt
 * <p>
 * ##########                 How to use it                ##########
 * Check the demo in main function
 */
@Slf4j
public abstract class CipherUtil {
    public static final String AES = "AES";
    public static final String DES = "DES";
    public static final String RSA = "RSA";
    private static final int RSA_LIMIT = 200;

    protected Key key;
    protected String charset;

    private CipherUtil() {
    }

    public static CipherUtil build(String type, String password, String charset) {
        if (AES.equalsIgnoreCase(type)) {
            return CipherAES.buildAES(password, charset);
        } else if (DES.equalsIgnoreCase(type)) {
            return CipherDES.buildDES(password, charset);
        } else if (RSA.equalsIgnoreCase(type)) {
            return CipherRSA.buildRSA(password, charset);
        }
        return null;
    }

    public abstract String encryptString(String str);

    public abstract String decryptString(String str);

    private static class CipherAES extends CipherUtil {
        private static CipherUtil buildAES(String password, String charset) {
            CipherAES cipherAES = new CipherAES();
            try {
                log.info("构造aes,charset:{},password:{}", charset, password);
                KeyGenerator kGen = KeyGenerator.getInstance(AES);
                SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
                secureRandom.setSeed(password.getBytes());
                kGen.init(128, secureRandom);
                SecretKey secretKey = kGen.generateKey();
                byte[] enCodeFormat = secretKey.getEncoded();
                cipherAES.key = new SecretKeySpec(enCodeFormat, AES);
                cipherAES.charset = charset;
            } catch (Exception e) {
                log.info("buildAES:" + e);
            }
            return cipherAES;
        }

        private String encryptAES(String s) {
            try {
                Cipher cipher = Cipher.getInstance(AES);
                cipher.init(Cipher.ENCRYPT_MODE, key);// 初始化
                byte[] byteContent = s.getBytes(charset);
                byte[] bytes = cipher.doFinal(byteContent);
                return new String(Base64.getUrlEncoder().encode(bytes), charset).trim();
            } catch (Exception e) {
                log.info("encryptAES:" + e);
                return null;
            }
        }

        private String decryptAES(String s) {
            try {
                Cipher cipher = Cipher.getInstance(AES);
                cipher.init(Cipher.DECRYPT_MODE, key);// 初始化
                byte[] data = s.getBytes(charset);
//                log.info("aes解密前的string：{}",s);
                byte[] result = cipher.doFinal(org.apache.commons.codec.binary.Base64.decodeBase64(data));
                return new String(result, charset).trim();
            } catch (Exception e) {
                log.info("decryptAES:" + e);
                return null;
            }
        }

        @Override
        public String encryptString(String str) {
            return encryptAES(str);
        }

        @Override
        public String decryptString(String str) {
            return decryptAES(str);
        }
    }

    private static class CipherDES extends CipherUtil {

        private SecureRandom sr;

        private static CipherUtil buildDES(String password, String charset) {
            CipherDES cipherDES = new CipherDES();
            try {
                DESKeySpec dks = new DESKeySpec(password.getBytes(charset));
                SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DES);
                cipherDES.sr = new SecureRandom();
                cipherDES.key = keyFactory.generateSecret(dks);
                cipherDES.charset = charset;
            } catch (Exception e) {
                log.info("buildDES:" + e);
            }
            return cipherDES;
        }

        @Override
        public String encryptString(String str) {
            try {
                Cipher cipher = Cipher.getInstance(DES);
                cipher.init(Cipher.ENCRYPT_MODE, key, sr);
                byte[] bytes = cipher.doFinal(str.getBytes(charset));
                return new String(Base64.getUrlEncoder().encode(bytes), charset).trim();
            } catch (Exception e) {
                log.info("encryptString:" + e);
                return null;
            }
        }

        @Override
        public String decryptString(String str) {
            try {
                Cipher cipher = Cipher.getInstance(DES);
                cipher.init(Cipher.DECRYPT_MODE, key, sr);
                byte[] result = cipher.doFinal(Base64.getUrlDecoder().decode(str.getBytes(charset)));
                return new String(result, charset).trim();
            } catch (Exception e) {
                log.info("decryptString:" + e);
                return null;
            }
        }
    }

    private static class CipherRSA extends CipherUtil {

        KeyFactory keyFactory;

        private static CipherUtil buildRSA(String password, String charset) {
            CipherRSA cipherRSA = new CipherRSA();
            try {
                byte[] bytes = Base64.getDecoder().decode(password.getBytes(charset));
                cipherRSA.keyFactory = KeyFactory.getInstance(RSA);
                Key key = cipherRSA.loadPublicKey(bytes);// Try to treated as Public Key
                if (key == null) {//Not a valid Public Key
                    key = cipherRSA.loadPrivateKey(bytes);// Try to treated as Private Key
                    if (key == null) {//Neither a valid Private Key
                        return null;
                    }
                }
                cipherRSA.key = key;
                cipherRSA.charset = charset;
            } catch (Exception e) {
                log.info("buildRSA:" + e);
                return null;
            }
            return cipherRSA;
        }

        private Key loadPublicKey(byte[] bytes) {
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(bytes);
            try {
                return keyFactory.generatePublic(keySpec);
            } catch (InvalidKeySpecException e) {
                log.info("loadPublicKey:" + e);
                return null;
            }
        }

        private Key loadPrivateKey(byte[] bytes) {
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(bytes);
            try {
                return keyFactory.generatePrivate(keySpec);
            } catch (InvalidKeySpecException e) {
                log.info("loadPrivateKey:" + e);
                return null;
            }
        }

        @Override
        public String encryptString(String str) {
            try {
                Cipher cipher = Cipher.getInstance(RSA);
                cipher.init(Cipher.ENCRYPT_MODE, key);
                if (str.length() > RSA_LIMIT) {
                    KeyGenerator keyGen = KeyGenerator.getInstance("AES");
                    keyGen.init(256); // for example
                    byte[] aesKey = keyGen.generateKey().getEncoded();
                    byte[] bytes = cipher.doFinal(aesKey);
                    String key = new String(Base64.getUrlEncoder().encode(bytes), charset);
                    CipherUtil cipherAES = CipherAES.buildAES(new String(Base64.getUrlEncoder().encode(aesKey), charset), charset);
                    String data = cipherAES.encryptString(str);
                    return key + "." + data;
                } else {
                    byte[] bytes = cipher.doFinal(str.getBytes(charset));
                    return new String(Base64.getUrlEncoder().encode(bytes), charset).trim();
                }
            } catch (Exception e) {
                log.info("encryptString:" + e);
                return null;
            }
        }

        @Override
        public String decryptString(String str) {
            try {
                Cipher cipher = Cipher.getInstance(RSA);
                cipher.init(Cipher.DECRYPT_MODE, key);
                int index;
                if ((index = str.indexOf('.')) != -1) {
//                    log.info("使用的charset是{}",charset);
                    byte[] aesKey = cipher.doFinal(Base64.getUrlDecoder().decode(str.substring(0, index).getBytes(charset)));
                    String aesKeyStr = new String(Base64.getUrlEncoder().encode(aesKey), charset);
//                    log.info("使用的aesKeyStr:{}",aesKeyStr);
                    CipherUtil cipherAES = CipherAES.buildAES(aesKeyStr, charset);
                    return cipherAES.decryptString(str.substring(index + 1));
                } else {
//                    log.info("直接解码");
                    byte[] result = cipher.doFinal(Base64.getUrlDecoder().decode(str.getBytes(charset)));
                    return new String(result, charset).trim();
                }
            } catch (Exception e) {
                log.info("decryptString:" + e);
                return null;
            }
        }
    }

    public static void main(String[] args) {
        try {
            CipherUtil cipherAESE = CipherUtil.build(AES, "rh6n8jaUURIQ1BjQnLbdnaZtr54SCQ7R", "UTF-8");
            CipherUtil cipherAESD = CipherUtil.build(AES, "rh6n8jaUURIQ1BjQnLbdnaZtr54SCQ7R", "UTF-8");
            String origin = "Hello world AES! 字符串";
            String encrypt = cipherAESE.encryptString("{\n" +
                    "\t\"package\": {\n" +
                    "\t\t\"body\": [{\n" +
                    "\t\t\t\"endDay\": \"2016-05-25\",\n" +
                    "\t\t\t\"hospitalId\": \"210004\",\n" +
                    "\t\t\t\"idCardNo\": \"12010419550525602X\",\n" +
                    "\t\t\t\"patientName\": \"张海璎\",\n" +
                    "\t\t\t\"patientType\": \"111\",\n" +
                    "\t\t\t\"startDay\": \"2016-05-25\"\n" +
                    "\t\t}],\n" +
                    "\t\t\"head\": {\n" +
                    "\t\t\t\"busenissType\": \"2\",\n" +
                    "\t\t\t\"sendTradeNum\": \"CI1524466766053\",\n" +
                    "\t\t\t\"senderCode\": \"ghrs\",\n" +
                    "\t\t\t\"senderName\": \"local\"\n" +
                    "\t\t}\n" +
                    "\t}\n" +
                    "}");
            String encrypttemp = cipherAESE.encryptString("{\"package\":{\"additionInfo\":{\"asyncAsk\":null,\"callback\":null,\"correlationId\":null,\"curDllAddr\":null,\"errorCode\":\"0\",\"errorMsg\":\"操作成功。\",\"receiverTradeNum\":null},\"body\":null,\"head\":{\"busenissType\":null,\"busseID\":null,\"clientmacAddress\":null,\"hosorgName\":null,\"hosorgNum\":null,\"receiverCode\":null,\"receiverName\":null,\"sendTradeNum\":null,\"senderCode\":null,\"senderName\":null,\"systemType\":null}}}");
            String decrypt = cipherAESD.decryptString("CjYyzkvuX8tBXWi-IDMWS1TveCxkCcB4ti2KrwdDR_Jq6QCJTEdd-VbOo2gLicBPTIJlfuoMWDjwkHo1yPjDVY41q1HDG6YpDRgWVWQuuPAr7dql1AS14FoRFySJ46UZNcPctuvLnXcga_6j8RVSp6Lv8I3RtQ1uLbuVgTrrQnvMVumNPq-JjoZ45LJFuZyUcb9YLJlYNJwWAxLb2NmQHS35KB9iCu5JXzXqxHmWNda5Eokgmg2tXBqJJ4Eyg3Z7mT5nYhig0d-FfsLEYoLoDU0377JqVzRYzI7DWz3JStgUUUtyKc06-wNKQBSFZ1tq5utogdJamuOoEK0v1AQWfOOqq3MOmdIU8gVHaCZL6dqmqYmFje-8GVMgHYAn5capC6iR56gbx34jvw4yECSS7kdbSY0JXtqnzrMSax1D7Opwss3tT8a6kbNDHCFiVxEM_cuTwYmVliCPz4VqTAatnA==");
            System.out.println(decrypt + origin.equals(decrypt));

            CipherUtil cipherDESE = CipherUtil.build(DES, "DES15#Gf^3H", "UTF-8");
            CipherUtil cipherDESD = CipherUtil.build(DES, "DES15#Gf^3H", "UTF-8");
            origin = "Hello world DES! 字符串";
            encrypt = cipherDESE.encryptString(origin);
            decrypt = cipherDESD.decryptString(encrypt);
            System.out.println(decrypt + origin.equals(decrypt));

            String pubKey =
                    "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxMtD9iONcWNn07Od7apz" +
                            "Ivny43w2CF4PVAUzKAeZrTDNIvy605nGecD9dGMp9YH7aSYg/nvOscPaIOFfBNcg" +
                            "PzXyXMQVu4utEgK6JPsG6bgCRVr8duQh2T4XIMl4Jt6JXmCfgnzuntn27Tx2bC8Y" +
                            "KTIVUucRBkWgNMXU88RPANy02jnqTERCQgIMy3VJ2IPGhnknZtmxy8qf7fnxOAim" +
                            "DkHLMWWvvlnbei02PHdhzFtCu7C7/HBU5XupmDsvous3DPLKwZlg3HSF7YWyWkDb" +
                            "XDYTAitP+MqKvxP2DgCzJ6vFu6dbV7Lp+zInNfQZRgwTUBcl67U2mtYIMNDmDvU0" +
                            "8QIDAQAB";

            String pemKey =
                    "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDEy0P2I41xY2fT" +
                            "s53tqnMi+fLjfDYIXg9UBTMoB5mtMM0i/LrTmcZ5wP10Yyn1gftpJiD+e86xw9og" +
                            "4V8E1yA/NfJcxBW7i60SArok+wbpuAJFWvx25CHZPhcgyXgm3oleYJ+CfO6e2fbt" +
                            "PHZsLxgpMhVS5xEGRaA0xdTzxE8A3LTaOepMREJCAgzLdUnYg8aGeSdm2bHLyp/t" +
                            "+fE4CKYOQcsxZa++Wdt6LTY8d2HMW0K7sLv8cFTle6mYOy+i6zcM8srBmWDcdIXt" +
                            "hbJaQNtcNhMCK0/4yoq/E/YOALMnq8W7p1tXsun7Mic19BlGDBNQFyXrtTaa1ggw" +
                            "0OYO9TTxAgMBAAECggEAHxviym4kVeBY8WZbeM6RFN5ePwfLotb7pVSNGa7Z5D7q" +
                            "rrU0825DqcuQRW3HnD/H9n9Ih/IaYo+LU9dMpf874cItZey5YsRZv5cexenI1fl4" +
                            "om4XqtO1ZhoindQ7YimbnYe4r3AAaKwcFLSSI9fuAdUg+dH9JviqsqzzVPWbZ481" +
                            "LDkLxvcykL26LJqSXPAAy8bAh3Hl/LQDJAPrS5+kL8Mbdc9de3e13ugqarqBhrCq" +
                            "Ww7vPbAwl7tHaRigKh7nt1qVKYnbMCCQKhKbyB1P24J77+w7efn73avM0TEM7MSX" +
                            "GEoIJr+XYci8EIAzKoHrgRSDtIrtL+rqXCUn3UL+YQKBgQDlpgKcLXIgcXGXMDSD" +
                            "DMnkZbZeB4pbgEMoTQ8FErIHie4fwg46WuG6I1BGU1+AEVUDb2SHvfPLuojBPuWY" +
                            "VJYsgoMvQEkfS3khFJlPyrsXBkg652BXsX3ZOaULdogiu7/Tv6cv1+8L3/sv9D/h" +
                            "joFeNgigp/QZZstgLgNXn1Hg0wKBgQDbYCVQ4z6fWF2VLZ6BBpPJUvvPi4fwIJUc" +
                            "fvB5gDM9Et+/YNdHQiFQbBBNV989Onndv4Pk29tExXENAozpRtfcuKMXWc0TQmi0" +
                            "pXUJiSttIxELo/se3vxKJC5o/FeY+dImpTCXajlWdiVs2QYqZ7/u8uo569GtPoQs" +
                            "4J2eMObYqwKBgFLGIY61erti6HR8NZE+3M5sMu11flM4yfwOumkzseOxvZjI02QJ" +
                            "Oe9jpx2SE4wPszrHvc6HkwZNt9kfmJXtdyuX5zzyyOh0xbY8vH3cLs8/vm0s1LcH" +
                            "DLttYaXeFXefgKcMH3DJPipjkyBXw2bDfLG+fCGcKAm32XpTW3ULwfFtAoGAUu5C" +
                            "zRRLDSPzpLxtKiuZXMdBKFGAHzfCBta6A8SCGjlzWj66w1Ztj6B6fTKV0uizPy/y" +
                            "JWczugjF1vw61skQO4GVLfOlwf0d5gSSfPtc7lIOVI82FJKJX2HUJZ4XY141oqRZ" +
                            "/TOL33KkbUSRvyAff65W+3R5fHugrfJvw2iLLCcCgYBSsPQGbpZAP9VhIg1zGmzP" +
                            "Sje9e+osRWcgqrKdplwXUMiQJeOxGG+axEtAtCp8x6KnyKqq79QDtvCjNBq5pKV2" +
                            "xcefZLoZ9ryglShM4QDiHgRX9dGb6/xIfUCs81MZdVts0cU4Q+3ZvwCfxWJ9JFv1" +
                            "885V34b1oW7u+7+TLWuDxA==";

            CipherUtil cipherRSAE = CipherUtil.build(RSA, pubKey, "UTF-8");
            CipherUtil cipherRSAD = CipherUtil.build(RSA, pemKey, "UTF-8");
            origin = "Hello world RSA! 字符串";
            encrypt = cipherRSAE.encryptString(origin);
            decrypt = cipherRSAD.decryptString(encrypt);
            System.out.println(decrypt + origin.equals(decrypt));
            origin = "Hello world RSA! Huge 字符串 1111111111111111111122222222222222222222" +
                    "3333333333333333333344444444444444444444555555555555555555556666666666666666666677777777777777777777" +
                    "888888888888888888889999999999999999999900000000000000000000";
            encrypt = cipherRSAE.encryptString(origin);
            decrypt = cipherRSAD.decryptString(encrypt);
            System.out.println(decrypt + origin.equals(decrypt));

            KeyGenerator keyGen = KeyGenerator.getInstance("DES");
            keyGen.init(56); // for example
            byte[] desKey = keyGen.generateKey().getEncoded();
            String key = new String(Base64.getUrlEncoder().encode(desKey), "UTF-8");
            System.out.println(key);

        } catch (Exception e) {
            log.info("test" + e);
        }
    }

}

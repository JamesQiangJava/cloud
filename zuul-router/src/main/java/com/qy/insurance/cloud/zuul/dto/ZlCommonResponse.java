package com.qy.insurance.cloud.zuul.dto;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description 极速赔 Response
 * @Author yangshuai
 * @Date 2018/4/25
 * @ModifiedBy
 * @Version v1.0
 */
@Data
public class ZlCommonResponse<T> {
    /**
     * 社商接收成功标志
     */
    private String receiveSign;
    /**
     * 是否成功
     */
    private boolean success;
    /**
     * 结果编码
     */
    private String resultCode;
    /**
     * 编码信息
     */
    private String message;
    /**
     * 返回时间戳
     */
    private String time;
    /**
     * 返回数据对象
     */
    private T data;

    /**
     * @Description 极速赔成功结果
     * @Author yangshuai
     * @Date 2018/5/29
     * @Param [t, successMessage]
     * @Return com.qy.insurance.cloud.zuul.dto.ZlCommonResponse<T>
     * @ModifiedBy
     * @Exception
     */
    public static <T> ZlCommonResponse<T> makeSuccessResult(T t, String successMessage) {
        ZlCommonResponse response = new ZlCommonResponse();
        response.setResultCode("0");
        response.setSuccess(true);
        response.setMessage(successMessage);
        response.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        response.setData(t);
        return response;
    }

    /**
     * @Description 极速赔失败结果
     * @Author yangshuai
     * @Date 2018/5/29
     * @Param [t, errorMessage]
     * @Return com.qy.insurance.cloud.zuul.dto.ZlCommonResponse<T>
     * @ModifiedBy
     * @Exception
     */
    public static <T> ZlCommonResponse<T> makeErrorResult(T t, String errorMessage) {
        ZlCommonResponse response = new ZlCommonResponse();
        response.setResultCode("2000005");
        response.setSuccess(false);
        response.setMessage(errorMessage);
        response.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        response.setData(t);
        return response;
    }

    /**
     * @Description 极速赔响应结果
     * @Author yangshuai
     * @Date 2018/5/29
     * @Param [commonResponse]
     * @Return java.lang.String
     * @ModifiedBy
     * @Exception
     */
    public static String toPackage(ZlCommonResponse<List> commonResponse) {
        CommonDto dto = new CommonDto();
        HeadDto headDto = new HeadDto();
        AdditionInfoDto additionInfoDto = new AdditionInfoDto();
        additionInfoDto.setErrorCode(commonResponse.getResultCode());
        additionInfoDto.setErrorMsg(commonResponse.getMessage());
        dto.setAdditionInfo(additionInfoDto);
        dto.setHead(headDto);
        dto.setBody(commonResponse.getData());
        Map<String, CommonDto> map = new HashMap<>();
        map.put("package", dto);
        String json = JSONObject.toJSONString(map, SerializerFeature.WriteMapNullValue);
        return json;
    }
}

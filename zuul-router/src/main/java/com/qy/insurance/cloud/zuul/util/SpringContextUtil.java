package com.qy.insurance.cloud.zuul.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @Description 获取spring上下文
 * @Author yangshuai
 * @Date 2018/5/22
 * @ModifiedBy
 * @Version v1.0
 */
@Component
@Slf4j
public class SpringContextUtil implements ApplicationContextAware {

    private static ApplicationContext ctx;

    /**
     * 实现ApplicationContextAware接口的回调方法，设置上下文环境
     *
     * @param applicationContext
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        SpringContextUtil.ctx = applicationContext;
    }

    /**
     * @return ApplicationContext
     */
    public static ApplicationContext getApplicationContext() {
        return ctx;
    }

    /**
     * 通过spring配置文件中配置的bean id取得bean对象
     *
     * @param id spring bean ID值
     * @return spring bean对象
     */
    public static Object getBean(String id) {
        if (ctx == null) {
            throw new NullPointerException("ApplicationContext is null");
        }
        Object obj = null;
        try {
            obj = ctx.getBean(id);
        } catch (NoSuchBeanDefinitionException e) {
            log.error(e.getMessage(), e);
        }
        return obj;
    }

    /**
     * 通过spring配置文件中配置的bean id取得bean对象
     *
     * @param clazz bean class
     * @return spring bean对象
     */
    public static <T> T getBean(Class<T> clazz) {
        if (ctx == null) {
            throw new NullPointerException("ApplicationContext is null");
        }
        T obj = null;
        try {
            obj = ctx.getBean(clazz);
        } catch (NoSuchBeanDefinitionException e) {
            log.error(e.getMessage(), e);
        }
        return obj;
    }
}

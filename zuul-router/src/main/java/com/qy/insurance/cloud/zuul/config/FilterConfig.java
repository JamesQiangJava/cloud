package com.qy.insurance.cloud.zuul.config;

import com.qy.insurance.cloud.zuul.filter.JwtFilter;
import com.qy.insurance.cloud.zuul.filter.RateLimitFilter;
import com.qy.insurance.cloud.zuul.filter.TestFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @task:
 * @discrption:
 * @author: Aere
 * @date: 2017/2/20 19:21
 * @version: 1.0.0
 */
@Configuration
public class FilterConfig {
    @Bean
    public TestFilter testFilter() {
        return new TestFilter();
    }

    @Bean
    public JwtFilter jwtFilter() {
        return new JwtFilter();
    }

    @Bean
    public RateLimitFilter rateLimitFilter() {
        //rateLimitFilter(route阶段，执行顺序为9)之后，RibbonRoutingFilter(route阶段，执行顺序为10)之前，不能再添加过滤器，减小限流误差
        return new RateLimitFilter();
    }
}

package com.qy.insurance.cloud.zuul.dto;

import lombok.Data;

import java.util.List;

/**
 * @Description
 * @Author yangshuai
 * @Date 2018/5/28
 * @ModifiedBy
 * @Version v1.0
 */
@Data
public class CommonDto {
    private HeadDto head;
    private List body;
    private AdditionInfoDto additionInfo;
}

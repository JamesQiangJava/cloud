package com.qy.insurance.cloud.zuul.service;

import com.alibaba.fastjson.JSON;
import com.qy.insurance.cloud.zuul.entity.RequestInfo;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * @Author yangshuai
 * @Date 2018/5/26
 * @ModifiedBy
 * @Version v1.0
 */
@Component
@Slf4j
@Data
public class ConsumeService {

    private String oauth2Token;

    @Autowired
    private OkHttpService okHttpService;

    @Value("${tokenService.url}")
    private String tokenServiceUrl;

    @Value("${tokenService.authorization}")
    private String authorization;

    @Value("${tomcat-config.port}")
    int port;

    /**
     * @Description 根据上下文发送请求
     * @Author yangshuai
     * @Date 2018/5/29
     * @Param [requestInfo]
     * @Return void
     * @ModifiedBy
     * @Exception
     */
    public void consumeRequest(RequestInfo requestInfo) {
        String serviceId = requestInfo.getServiceId();
        int newTimes = 1;
        //解析当前重调次数
        String currentTimesStr = requestInfo.getZuulTimes();
        if (currentTimesStr != null) {
            int currentTime = Integer.parseInt(currentTimesStr);
            if (currentTime > 3) {
                log.info("{}的一个请求次数超过3次，该请求从队列中退出", serviceId);
                return;
            } else {
                newTimes = ++currentTime;
            }
        }
        String uri = requestInfo.getUri();
        uri = uri.replace("//", "/");
        StringBuilder realUrl = new StringBuilder("http://");
        realUrl.append("localhost:").append(this.port);
        realUrl.append(uri).append("?").append("zuulTimes=").append(newTimes);
        log.info("url:{}", realUrl.toString());
        //准备头部信息，如token
        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", this.getToken());
        okHttpService.postJsonParams(realUrl.toString(), requestInfo.getRequestJson(), headerMap);
    }


    /**
     * @Description 获取token定时器
     * @Author yangshuai
     * @Date 2018/5/29
     * @Param []
     * @Return void
     * @ModifiedBy
     * @Exception
     */
    @Scheduled(fixedDelay = 25200000)
    private void requestToken() {
        //准备头部信息，如token
        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", this.authorization);
        //获取Token
        String tokenJson = okHttpService.postJsonParams(this.tokenServiceUrl, "", headerMap);
        if (StringUtils.isEmpty(tokenJson)) {
            log.error("从oauth2获取token失败");
            return;
        }
        StringBuilder token = new StringBuilder("bearer ");
        //解析response
        Map<String, String> responseMap = JSON.parseObject(tokenJson, Map.class);
        if (responseMap != null) {
            if (responseMap.containsKey("access_token")) {
                String tokenBody = responseMap.get("access_token");
                token.append(tokenBody);
            }
        }
        log.info("从{}获取token成功,token为：{}", this.tokenServiceUrl, token);
        this.oauth2Token = token.toString();
    }

    /**
     * @Description 获取token
     * @Author yangshuai
     * @Date 2018/5/29
     * @Param []
     * @Return java.lang.String
     * @ModifiedBy
     * @Exception
     */
    public String getToken() {
        if (this.oauth2Token == null) {
            this.requestToken();
        }
        return this.oauth2Token;
    }


}

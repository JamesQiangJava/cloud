package com.qy.insurance.cloud.zuul.entity;

import lombok.Data;

/**
 * @Description HttpRequest信息类
 * @Author yangshuai
 * @Date 2018/5/29
 * @ModifiedBy
 * @Version v1.0
 */
@Data
public class RequestInfo {
    private String serviceId;
    private String uri;
    private String requestJson;
    private String zuulTimes;
}

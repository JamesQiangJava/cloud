package com.qy.insurance.cloud.zuul.dto;

import lombok.Data;

/**
 * @Description
 * @Author yangshuai
 * @Date 2018/5/28
 * @ModifiedBy
 * @Version v1.0
 */
@Data
public class HeadDto {
    /**
     * 业务编码
     */
    private String busseID;
    /**
     * 发送方交易流水号
     */
    private String sendTradeNum;
    /**
     * 发起方编号
     */
    private String senderCode;
    /**
     * 发起方名称
     */
    private String senderName;
    /**
     * 接收方编号
     */
    private String receiverCode;
    /**
     * 接收方名称
     */
    private String receiverName;
    /**
     * 操作员编号
     */
    private String hosorgNum;
    /**
     * 操作员名称
     */
    private String hosorgName;
    /**
     * 系统类型
     */
    private String systemType;
    /**
     * 业务类型
     */
    private String busenissType;
    /**
     * 客户端mac地址
     */
    private String clientmacAddress;
}

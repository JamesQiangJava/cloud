package com.qy.insurance.cloud.zuul.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.qy.insurance.cloud.zuul.dto.CommonResponse;
import com.qy.insurance.cloud.zuul.entity.RequestInfo;
import com.qy.insurance.cloud.zuul.service.OkHttpService;
import com.qy.insurance.cloud.zuul.service.RateLimitService;
import com.qy.insurance.cloud.zuul.util.RequestHandUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.InetAddress;
import java.util.Calendar;

/**
 * @Description 限速拦截器
 * @Author yangshuai
 * @Date 2018/5/18
 * @ModifiedBy
 * @Version v1.0
 */
@Slf4j
public class RateLimitFilter extends ZuulFilter {

    @Autowired
    RateLimitService rateLimitService;

    String encryptKey = "rh6n8jaUURIQ1BjQnLbdnaZtr54SCQ7R";

    @Autowired
    private OkHttpService okHttpService;

    @Override
    public String filterType() {
        return "route";
    }

    @Override
    public int filterOrder() {
        //执行顺序为 PreDecorationFilter之后，否则requestContext里拿不到serviceId等数据
        return 9;
    }

    @Override
    public boolean shouldFilter() {
        RequestContext context = RequestContext.getCurrentContext();
        HttpServletRequest request = context.getRequest();
        String serviceId = (String) context.get("serviceId");
        String requestURI = request.getRequestURI();
        //来自网关自身的请求不拦截
        String zuulTimes = request.getParameter("zuulTimes");
        String localAddr = "";
        try {
            localAddr = InetAddress.getLocalHost().getHostAddress();
        } catch (Exception e) {
            log.error("获取本机IP失败", e);
        }
        String clientAddr = request.getRemoteAddr();
        if ((clientAddr.equals("127.0.0.1") || clientAddr.equals("localhost") || clientAddr.equals(localAddr)) && !StringUtils.isEmpty(zuulTimes)) {
            return false;
        }
        if ("insurance-lp-zl".equals(serviceId) && requestURI.endsWith("/getPatientData")) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String clientCode = authentication.getPrincipal().toString();
            log.info("通过token验证出客户code是{}", clientCode);
            if (!StringUtils.isEmpty(clientCode) && clientCode.equals("bjs")) {
                Calendar rightNow = Calendar.getInstance();
                int hour = rightNow.get(Calendar.HOUR_OF_DAY);
                if (hour >= 0 && hour < 24) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public Object run() {
        //获取当前路由的服务
        RequestContext context = RequestContext.getCurrentContext();
        HttpServletResponse response = context.getResponse();
        String serviceId = (String) context.get("serviceId");
        //请求解析
        RequestInfo requestInfo = RequestHandUtil.getRequestInfo(context);
        //加入队列处理
        rateLimitService.addRequest(requestInfo);
        pushCommonResponse(serviceId, response);

        //zuul过滤不通过
        context.setSendZuulResponse(false);
        return null;
    }


    /**
     * @Description 构造降级处理响应
     * @Author yangshuai
     * @Date 2018/5/29
     * @Param [uri, serviceId, response]
     * @Return void
     * @ModifiedBy
     * @Exception
     */
    public void pushCommonResponse(String serviceId, HttpServletResponse response) {
        //构造响应
        HttpStatus httpStatus = HttpStatus.OK;
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setCharacterEncoding("UTF-8");
        response.setStatus(httpStatus.value());
        //TODO 获取参数校验结果
        String responseJson = CommonResponse.getResponseJson(true, serviceId);
        //响应
        try {
            response.getWriter().append(responseJson);
        } catch (Exception e) {
            log.info("降级处理，构造response异常");
        }
    }
}
